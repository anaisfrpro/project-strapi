API_URL="http://localhost:1337/api" 
TOTAL_REQUESTS=200

make_request() {
  local endpoint=$1
  local method=$2
  local data=$3

  curl -X $method -s "$API_URL/$endpoint" \
    -H "Content-Type: application/json" \
    -d "$data"
}

for ((i = 1; i <= TOTAL_REQUESTS; i++)); do

  make_request "articles" "GET"

  make_request "articles" "POST" '{"Title": "Nouvel article test", "Body": "Contenu de l'\''article test"}'

  make_request "articles/1" "PUT" '{"Body": "Contenu mis à jour"}'

  make_request "articles/1" "DELETE"

  echo "Request $i sent"
done

echo "Load simulation completed"