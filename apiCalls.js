const axios = require('axios');

const API_URL = process.env.API_URL || "http://localhost:1337/api/articles";
const TOTAL_REQUESTS = process.env.TOTAL_REQUESTS || 1000 ;

async function createRequest() {
  try {
    const response = await axios.post(API_URL, {
      data: { Title: 'New Article', Body: 'Lorem Ipsum' }
    });

    console.log('Create Request Response:', response.data);
  } catch (error) {
    console.error('Error in create request:', error.message);
  }
}

async function readRequest() {
  try {
    const response = await axios.get(API_URL);

    console.log('Read Request Response:', response.data);
  } catch (error) {
    console.error('Error in read request:', error.message);
  }
}

async function updateRequest() {
  try {
    const articles = await axios.get(API_URL);
    const articleToUpdate = articles.data.data[0];

    if (articleToUpdate) {
      const response = await axios.put(`${API_URL}/${articleToUpdate.id}`, {
        data: { Title: 'Updated Article', Body: 'New Content' }
      });

      console.log('Update Request Response:', response.data);
    } else {
      console.error('No articles found for update');
    }
  } catch (error) {
    console.error('Error in update request:', error.message);
  }
}


async function deleteRequest() {
  try {
    const articles = await axios.get(API_URL);
    const articleToDelete = articles.data.data[0];

    if (articleToDelete) {
      const response = await axios.delete(`${API_URL}/${articleToDelete.id}`);

      console.log('Delete Request Response:', response.data);
    } else {
      console.error('No articles found for delete');
    }
  } catch (error) {
    console.error('Error in delete request:', error.message);
  }
}

async function simulateLoad() {
  // @ts-ignore
  for (let i = 1; i <= TOTAL_REQUESTS; i++) {
    switch (i % 4) {
      case 0:
         createRequest();
        break;
      case 1:
         readRequest();
        break;
      case 2:
         updateRequest() 
        break;
      case 2:
         deleteRequest();
        break;
    }

    console.log(`Request ${i} sent`);
  }

  console.log('Load simulation completed');
}

simulateLoad();
