const axios = require('axios');

const apiUrl = 'http://172.16.8.55:1337';
const numberOfCalls = 100;

async function makeApiCalls() {
  try {
    for (let i = 0; i < numberOfCalls; i++) {
      const response = await axios.get(apiUrl + '/api/articles');
      console.log(`Appel ${i + 1} - Status: ${response.status}, Temps de réponse: ms`);
    }
  } catch (error) {
    console.error('Erreur lors de l\'appel à l\'API :', error.message);
  }
}

makeApiCalls();